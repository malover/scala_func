//import scala.annotation.tailrec
//
//object potrjas extends App {
//
//  def remove(string: String, maxRepeat: Int): String = {
//
//    @tailrec
//    def rec(leftChars: List[Char], lastChar: Char, currentCount: Int, result: String): String = {
//      leftChars match {
//        case Nil => result
//        case head :: tail =>
//          if (head == lastChar) {
//            if (currentCount >= maxRepeat) {
//              rec(tail, lastChar, currentCount, result)
//            } else {
//              rec(tail, lastChar, currentCount + 1, result + head)
//            }
//          } else {
//            rec(tail, head, 1, result + head)
//          }
//      }
//    }
//
//    string.toCharArray.toList match {
//      case Nil => string
//      case head :: tail if maxRepeat=> rec(tail, head, 1, head.toString)
//    }
//  }
//
//  println(remove("abcd", 0))
//}
