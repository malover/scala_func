object chapter1 {
  val x = "Hello, World"
  val x_1 = "Hello, World".reverse
  val x_2 = x.reverse

  val y = new StringBuilder("Hello")

  val r1 = y.append(", World").toString
  val r2 = y.append(", World").toString // StringBuilder.append is not a pure function


case class Player1(name: String, score: Int) {
  def printWinner(p: Player1): Unit =
    println(p.name + " is the winner!")

  def declareWinner(p1: Player1, p2: Player1): Unit =
    if (p1.score > p2.score) printWinner(p1)
    else printWinner(p2)
}

case class Player2(name: String, score: Int) {
  def printWinner(p: Player2): Unit =
    println(p.name + " is the winner!")

  def winner(p1: Player2, p2: Player2): Player2 =
    if (p1.score > p2.score) p1 else p2

  def declareWinner(p1: Player2, p2: Player2): Unit =
    printWinner(winner(p1, p2))
}

  def main(cmdlineArgs: Array[String]): Unit = {
    //    println(chapter1.x)
    //    println(chapter1.x_1)
    //    println(chapter1.x_2)
    //
    //    println(r1)
    //    println(r2)

    val sue = Player1("Sue", 7)

    val bob = Player1("Bob", 8)

    sue.declareWinner(sue, bob)

  }
}




