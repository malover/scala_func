import chapter2.fibbonacchi

object chapter2 {

  def factorial(n: Int): Int = {
    @annotation.tailrec
    def go(n: Int, acc: Int): Int =
      if (n <= 0) acc
      else go(n-1, n*acc)
    go(n, 1)
  }

  /** Exercise 1
    *
    * Write a recursive function to get the nth fibonacci number. Your definition should use a local tail-recursive
    * function.
    */
  def fibbonacchi(n: Int): Int = {
    if (n<=1) n
    @annotation.tailrec
    def fib(x:Int, summ: Int): Int =
      if (n < x) summ
      else fib(x+1, summ + x)

    fib(0, 0)
  }

  /** Exercise 2
    *
    * Check isSorted, which checks whether an Array[A] is sorted according to a given comparison function.
    */
  def isSorted[A](as: Array[A], gt: (A,A) => Boolean): Boolean = {
    @annotation.tailrec
    def sorted(i: Int): Boolean ={
      if (i >= as.length - 1) true
      else !gt(as(i),as (i+1 )) && sorted (i+1)
    }
    sorted(0)
  }

  /** Exercise 3
    *
    * Implement partial1 and write down a concrete usage of it.
    */
  def partial1[A,B,C](a: A, f: (A,B) => C): B => C = {
    (b:B) => f(a, b)
  }


  /** Exercise 4
    *
    * currying, which converts a
    * function of N arguments into a function of one argument that returns another
    * function as its result
    */
  def curry[A,B,C](f: (A, B) => C): A => B => C = {
    (a: A) => (b: B) => f(a, b)
  }

  /**
    * EXERCISE 5 (optional): Implement uncurry, which reverses the
    * transformation of curry, converts function of one argument into a function of N arguments
    *
    */
  def uncurry[A,B,C](f: A => B => C): (A, B) => C = {
    (a:A, b:B) => f(a)(b)
  }

  /**
    * EXERCISE 6
    *
    * function composition, which feeds the output of
    * one function in as the input to another function.
    */
  def compose[A,B,C](f: B => C, g: A => B): A => C = {
    (a:A) => f(g(a))
  }

  def main(args: Array[String]): Unit =
  //    println(formatAbs(-42))
  //  println(factorial(5))
    println(fibbonacchi(5))
  //
  //    println(isSorted(Array(1,2,3),))
}


