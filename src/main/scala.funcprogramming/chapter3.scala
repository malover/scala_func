/**
  *
  */
object chapter3 {

  sealed trait List[+A]

  case object Nil extends List[Nothing]

  case class Cons[+A](head: A, tail: List[A]) extends List[A]

  object List {
    def sum(ints: List[Int]): Int = ints match {
      case Nil => 0
      case Cons(x, xs) => x + sum(xs)
    }

    def product(ds: List[Double]): Double = ds match {
      case Nil => 1.0
      case Cons(0.0, _) => 0.0
      case Cons(x, xs) => x * product(xs)
    }

    def apply[A](as: A*): List[A] =
      if (as.isEmpty) Nil
      else Cons(as.head, apply(as.tail: _*))

    def append[A](a1: List[A], a2: List[A]): List[A] =
      a1 match {
        case Nil => a2
        case Cons(h, t) => Cons(h, append(t, a2))
      }

    /**
      * EXERCISE 2: Implement the function tail for "removing" the first element
      * of a List.
      *
      */
    def tail[A](ls: List[A]): List[A] = {
      ls match {
        case Nil => Nil
        case Cons(x, t) => t
      }
    }

    /**
      * EXERCISE 3: Generalize tail to the function drop, which removes the first
      * n elements from a list.
      *
      */
    def drop[A](ls: List[A], n: Int): List[A] = {
      n match {
        case 0 => ls
        case _ => drop(tail(ls), n - 1)
      }
    }

    /**
      * EXERCISE 4: Implement dropWhile, which removes elements from the
      * List prefix as long as they match a predicate. Again, notice these functions take
      * time proportional only to the number of elements being dropped—we do not need
      * to make a copy of the entire List.
      *
      */
    def dropWhile[A](l: List[A])(f: A => Boolean): List[A] = l match {
      case Cons(h, t) if f(h) => dropWhile(t)(f)
      case _ => l
    }


    /**
      * EXERCISE 5: Using the same idea, implement the function setHead for
      * replacing the first element of a List with a different value.
      */
    def setHead[A](l: List[A], newH: A): List[A] = {
      Cons(newH, l)
    }

    /**
      * EXERCISE 6: Implement a function,
      * init, which returns a List consisting of all but the last element of a List. So,
      * given List(1,2,3,4), init will return List(1,2,3). Why can't this
      * function be implemented in constant time like tail?
      */
    def init[A](l: List[A]): List[A] = {
      l match {
        case Cons(h, Nil) => Nil
        case Cons(h, t) => Cons(h, init(t))
        case Nil => ???
      }
    }

    def foldRight[A, B](l: List[A], z: B)(f: (A, B) => B): B =
      l match {
        case Nil => z
        case Cons(h, t) => f(h, foldRight(t, z)(f))
      }

    def sum2(l: List[Int]) =
      foldRight(l, 0.0)(_ + _)

    def product2(l: List[Double]) =
      foldRight(l, 1.0)(_ * _)

    /**
      * Exercise 7
      * Can product, implemented using foldRight, immediately halt the recursion
      * and return 0.0 if it encounters a 0.0? Why or why not? Consider how any
      * short-circuiting might work if you call foldRight with a large list. This
      * is a deeper question that we’ll return to in chapter 5.
      *
      * Answer: It can not because the function f has as of right now no way of
      * stopping the foldRight calls
      */

    /**
      * EXERCISE 8: See what happens when you pass Nil and Cons themselves to
      * foldRight, like this: foldRight(List(1,2,3),
      * Nil:List[Int])(Cons(_,_)).13 What do you think this says about the
      * relationship between foldRight and the data constructors of List?
      *
      */

    def experiment(): List[Int] = {
      List.foldRight(List(1, 2, 3), Nil: List[Int])(Cons(_, _))
    }

    /**
      * EXERCISE 9: Compute the length of a list using foldRight.
      *
      */

    def length[A](l: List[A]): Int = {

      def matchElement(el: A, length: Int) = {
        el match {
          case null => 0 + length
          case _ => 1 + length
        }
      }

      List.foldRight(l, 0)(matchElement)
    }

    def length2[A](l: List[A]): Int = {
      List.foldRight(l, 0)((_, z) => z + 1)
    }

    /**
      * EXERCISE 10: foldRight is not tail-recursive and will StackOverflow
      * for large lists. Convince yourself that this is the case, then write another general
      * list-recursion function, foldLeft that is tail-recursive, using the techniques we
      * discussed in the previous chapter.
      *
      */
    @annotation.tailrec
    def foldLeft[A, B](l: List[A], z: B)(f: (B, A) => B): B =
      l match {
        case Nil => z
        case Cons(h, t) => foldLeft(t, f(z, h))(f)
      }

    /**
      * EXERCISE 11: Write sum, product, and a function to compute the length of
      * a list using foldLeft.
      *
      */
    def sum3(l: List[Int]) =
      foldLeft(l, 0.0)(_ + _)

    def product3(l: List[Double]) =
      foldLeft(l, 1.0)(_ * _)

    def length3[A](l: List[A]): Int = {
      def matchElement(length: Int, el: A) = {
        el match {
          case null => 0 + length
          case _ => 1 + length
        }
      }

      foldLeft(l, 0)(matchElement)
    }

    def length33[A](l: List[A]): Int = {
      foldLeft(l, 0)((x, _) => x + 1)
    }

    /**
      * EXERCISE 12: Write a function that returns the reverse of a list (so given
      * List(1,2,3) it returns List(3,2,1)). See if you can write it using a fold.
      *
      */

    def reverse[A](l: List[A]): List[A] = {
      foldLeft(l, Nil: List[A])((x, y) => Cons(y, x))
    }

    /**
      * EXERCISE 13 (hard): Can you write foldLeft in terms of foldRight?
      * How about the other way around?
      */
    //  def foldLeft[A](l: List[A], z: A): List[A]= {
    //      foldRight(l, z)
    //  }

    //  def append[A](a1: List[A], a2: List[A]): List[A] =
    //  a1 match {
    //    case Nil => a2
    //    case Cons(h, t) => Cons(h, append(t, a2))
    //  }

    /**
      * EXERCISE 14: Implement append in terms of either foldLeft or
      * foldRight.
      */
    def append2[A](a1: List[A], a2: List[A]): List[A] = {
      foldLeft(a1, a2)((a, b) => Cons(b, a))
    }

    /** EXERCISE 15 (hard): Write a function that concatenates a list of lists into a
      * single list. Its runtime should be linear in the total length of all lists.
      * Try to use functions we have already defined.
      */
    def concat[A](ns: List[List[A]]): List[A] = foldLeft(ns, List[A]())((b, a) => append(b, a))

    /**
      * EXERCISE 16: Write a function that transforms a list of integers by adding 1
      * to each element. (Reminder: this should be a pure function that returns a new
      * List!)
      *
      */

    def add1(l: List[Int]): List[Int] = {
      foldRight(l, Nil: List[Int])((h, t) => Cons(h + 1, t))
    }

    /**
      * EXERCISE 17: Write a function that turns each value in a List[Double]
      * into a String.
      */

    def doubleIntoString(l: List[Double]): List[String] = {
      foldRight(l, Nil: List[String])((h, t) => Cons(h.toString, t))
    }

    /**
      * EXERCISE 18: Write a function map, that generalizes modifying each element
      * in a list while maintaining the structure of the list.
      *
      * @param args
      */
    def map[A, B](l: List[A])(f: A => B): List[B] = {
      foldLeft(l, List[B]())((b,a) => Cons(f(a), b))
    }

    /**
      * EXERCISE 19: Write a function filter that removes elements from a list
      * unless they satisfy a given predicate. Use it to remove all odd numbers from a
      * List[Int].
      *
      */
    def filter[A](l: List[A])(f: A => Boolean): List[A] = {
      foldRight(l, Nil: List[A])((h, t) => {
        if (f(h)) Cons(h, t)
        else t
      })
    }

    def removeOddNumbers(l: List[Int]): List[Int] = {
      filter(l)((el: Int) => el % 2 != 0)
    }

    /**
      * EXERCISE 20: Write a function flatMap, that works like map except that
      * the function given will return a list instead of a single result, and that list should be
      * inserted into the final resulting list.
      *
      */
    def flatMap[A, B](l: List[A])(f: A => List[B]): List[B] = {
      concat(map(l)(f))
    }

    /**
      * EXERCISE 21: Can you use flatMap to implement filter
      *
      */
    def filter2[A](l: List[A])(f: A => Boolean): List[A] = {
      flatMap(l)((x: A) => if (f(x)) List(x) else Nil)
    }

    /**
      * EXERCISE 22: Write a function that accepts two lists and constructs a new list
      * by adding corresponding elements. For example, List(1,2,3) and
      * List(4,5,6) becomes List(5,7,9).
      */

    def addLists(l1: List[Int], l2: List[Int]): List[Int] = (l1, l2) match {
      case (Nil, Nil) => Nil
      case (_, Nil) => l1
      case (Nil, _) => l2
      case (Cons(h1, t1), Cons(h2, t2)) => Cons(h1+h2, addLists(t1, t2))
      }
    }

  /**
    * 
    * @param args
    */


  //    val example = Cons(1, Cons(2, Cons(3, Nil)))
  //    val example2 = List(1,2,3)
  //    val total = sum(example)


  def main(args: Array[String]): Unit = {
    //        println(List.length2(List(1,2,3,4,5,6,7)))
    //    println(length2(List()))
    //
    //    scala.List.range(1, 1000000000).foldRight(0)(_ + _)

    //    println(List.foldRight(List(1,2,3),4)(_+_))

    //    println(List.foldLeft(List(1,2,3),4)(_+_))

    //        println(List.doubleIntoString(List(1.5,1.6,2.5)))
    //println(experiment())
    //    println(5)

    //  val x = List(1,2,3,4,5) match {
    //    case Cons(x, Cons(2, Cons(4, _))) => x
    //    case Nil => 42
    //    case Cons(x, Cons(y, Cons(3, Cons(4, _)))) => x + y
    //    case Cons(h, t) => h + sum(t)
    //    case _ => 101
    //  }
    //        println(List.flatMap(List(1, 2, 3))(i => List(i,i)))

    //    val victor = List.concat(List(List(1,2)))
    //
    //    println(victor)

    println(List.addLists(List(1,2,3,4,5), List(1,2,3,4,5)))
  }
}
