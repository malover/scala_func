object chapter3_2 {

  sealed trait Tree[+A]

  case class Leaf[A](value: A) extends Tree[A]

  case class Branch[A](left: Tree[A], right: Tree[A]) extends Tree[A]

  /**
    * EXERCISE 25: Write a function size that counts the number of nodes in a
    * tree.
    */

  object Tree {
    //@tailrec
    def size[A](tree: Tree[A]): Int = tree match {

      case Leaf(_) => 1
      case Branch(l, r) => size(l) + size(r)
    }

    /**
      * EXERCISE 26: Write a function maximum that returns the maximum element
      * in a Tree[Int]. (Note: in Scala, you can use x.max(y) or x max y to
      * compute the maximum of two integers x and y.)
      */
    def max(tree: Tree[Int]): Int = tree match {
      case Leaf(v) => v
      case Branch(l, r) => max(l) max max(r)
    }

    /**
      * EXERCISE 27: Write a function depth that returns the maximum path length
      * from the root of a tree to any leaf.
      */


    def depth(tree: Tree[Int]): Int = tree match {
      case Leaf(v) => 0
      case Branch(l, r) => depth(l) + depth(r) + 1
    }

    /**
      * EXERCISE 28: Write a function map, analogous to the method of the same
      * name on List, that modifies each element in a tree with a given function.
      */

    def map[A, B](tree: Tree[A])(f: A => B): Tree[B] = tree match {
      case Leaf(v) => Leaf(f(v))
      case Branch(l, r) => Branch(map(l)(f), map(r)(f))
    }

    /**
      * EXERCISE 29: Generalize size, maximum, depth, and map, writing a new
      * function fold that abstracts over their similarities. Reimplement them in terms of
      * this more general function.
      */
    def fold[A, B](tree: Tree[A])(f: A => B)(g: (B, B) => B): B = tree match {
      case Leaf(v) => f(v)
      case Branch(l, r) => g(fold(l)(f)(g), fold(r)(f)(g))
    }

    def size2[A](tree: Tree[A]): Int = {
      fold[A, Int](tree)(_ => 1)(_ + _)
    }

    def max2(tree: Tree[Int]): Int = {
      fold[Int, Int](tree)(v => v)(_ max _)
    }

    def depth2(tree: Tree[Int]): Int = {
      fold[Int, Int](tree)(_ => 0)(_ + _ + 1)
    }

    def map2[A, B](tree: Tree[A])(f: A => B): Tree[B] = {
      fold[A, Tree[B]](tree)(v => Leaf(f(v)))((l, r) => Branch(l, r))
    }
  }

    def main(args: Array[String]): Unit = {

      val vladlen = new Branch[Int](new Leaf(8), new Leaf(2))
      val simeon = new Branch[Int](new Leaf(3), new Leaf(5))
      val hristo = new Branch[Int](vladlen, simeon)
      println("Tree size is: " + Tree.size(hristo))
      println("Max tree element is: " + Tree.max(hristo))
      println("Tree depth is: " + Tree.depth(hristo))
      println("Applied map is: " + Tree.map(hristo)(_ * 100))
      println("Tree size2 is: " + Tree.size2(hristo))
      println("Max2 tree element is: " + Tree.max2(hristo))
      println("Tree depth2 is: " + Tree.depth2(hristo))
      println("Applied map2 is: " + Tree.map2(hristo)(_ * 100))

    }
}
